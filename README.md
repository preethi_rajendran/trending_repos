# Github Repositories #

## Overview
___

An Android Application that lists the most trending repositories in Android from Github. Users can filter the repositories by its name and description.


## Application Configuration ##
___

1. **applicationId:** com.github.listx
2. **min-sdk:** 23
3. **versionCode:** 1
4. **versionName:** 1.0


## Application Architecture and Tech Stack ##
___

Application uses **MVVM** architecture and MVVM pattern. 


Kotlin based + Coroutines for asynchronous process.

JetPack
* LiveData - notify domain layer data to views.
* ViewModel - UI related data holder, lifecycle aware.
* Data Binding - declaratively bind observable data to UI elements.
* Retrofit2 & OkHttp3 - construct the REST APIs and fetch network data.
* Room - store data in database for offline support

Glide - loading images.

## Application Features ##
___

1. View the most trending repositories from Github. 
2. Offline support
3. Search repostories by name and description
4. Pull to refresh (Fetches fresh data from remote)


## Contact ##
___
- [identifypreethi@gmail.com](mailto:identifypreethi@gmail.com)
- [+91-9025269144](tel:+919025269144)
