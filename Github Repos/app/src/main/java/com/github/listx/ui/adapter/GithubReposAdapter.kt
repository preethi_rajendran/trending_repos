package com.github.listx.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.github.listx.R
import com.github.listx.model.database.GithubRepo
import com.github.listx.databinding.ItemRepositoryBinding

/**
 *Adapter class for github trending repositories' recycler view
 */

class GithubReposAdapter : ListAdapter<GithubRepo, GithubReposAdapter.RepoViewHolder>(
    diffUtilCallBack
) {
    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        getItem(position)?.let { repo ->
            holder.repositoryBinding.repository = repo
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        return RepoViewHolder.create(parent)
    }

    class RepoViewHolder(var repositoryBinding: ItemRepositoryBinding) :
        RecyclerView.ViewHolder(repositoryBinding.root) {
        companion object {
            fun create(parent: ViewGroup): RepoViewHolder {
                return RepoViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_repository,
                        parent,
                        false
                    )
                )
            }
        }
    }

    companion object {
        val diffUtilCallBack = object : DiffUtil.ItemCallback<GithubRepo>() {
            override fun areItemsTheSame(
                oldItem: GithubRepo,
                newItem: GithubRepo
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: GithubRepo,
                newItem: GithubRepo
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}