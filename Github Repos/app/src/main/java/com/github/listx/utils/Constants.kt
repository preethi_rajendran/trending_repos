package com.github.listx.utils

object Constants {
    const val API_BASE_URL = "https://api.github.com"
    const val APP_DATABASE_NAME = "github_repos_database"
    enum class State {
        LOADED, LOADING, NONE
    }
}