package com.github.listx.ui.adapter

import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.github.listx.R
import com.google.android.material.imageview.ShapeableImageView

@BindingAdapter("app:setImage")
fun setImage(view: ShapeableImageView, url: String) {
    Glide
        .with(view.context)
        .load(url)
        .centerCrop()
        .placeholder(R.drawable.ic_account_avatar)
        .into(view);
}