package com.github.listx.network

import com.github.listx.BuildConfig
import com.github.listx.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitHub {
    private var retrofit: Retrofit
    private const val TIMEOUT_CONNECTION = 60000L
    private const val TIMEOUT_READ = 60000L

    init {
        val okHttpClient = OkHttpClient().newBuilder().addInterceptor(getLoggingInterceptor())
            .readTimeout(TIMEOUT_READ, TimeUnit.MILLISECONDS)
            .connectTimeout(TIMEOUT_CONNECTION, TimeUnit.MILLISECONDS)
            .retryOnConnectionFailure(true)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(Constants.API_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    }

    val githubAPI by lazy { retrofit.create(GithubAPI::class.java) }

    private fun getLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return httpLoggingInterceptor
    }
}