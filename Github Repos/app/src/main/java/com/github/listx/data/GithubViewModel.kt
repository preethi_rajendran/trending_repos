package com.github.listx.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.github.listx.model.database.GithubRepo
import com.github.listx.utils.Constants
import kotlinx.coroutines.*

class GithubViewModel(application: Application) : AndroidViewModel(application) {

    var job = SupervisorJob()

    val scope = CoroutineScope(Dispatchers.Main + job)

    /**
     * Repository instance
     */
    private val repository by lazy { GithubRepository(application) }

    /**
     * Livedata for loading status
     */
    val loadingStatus: LiveData<Constants.State> = repository.getLoading()

    /**
     * Livedata for Error message
     */
    val errorMessage: MutableLiveData<String> = repository.getErrorMessage()

    /**
     * Mutable Livedata to hold data of trending repostories list
     */
    val repositoriesList = MutableLiveData<List<GithubRepo>>()

    /**
     * Mutable livedata to hold filtered repositories list
     */
    private var filteredRepositoriesList = MutableLiveData<MutableList<GithubRepo>>()

    /**
     * Mutable livedata of search query
     */
    var searchQueryLiveData = MutableLiveData<String>()

    /**
     * Get list of trending github repositories
     */
    fun getTrendingRepositories() {
        scope.launch {
            repositoriesList.value = withContext(Dispatchers.IO) {
                repository.getTrendingRepositories()
            }
        }
    }

    /**
     * Filtering out the list based on the search value.
     */
    fun filteredRepositoriesList() = Transformations.switchMap(searchQueryLiveData) {
        filteredRepositoriesList.apply {
            postValue(repositoriesList.value?.filter {
                it.name.lowercase().contains(searchQueryLiveData.value ?: "") ||
                        it.description?.lowercase()
                            ?.contains(searchQueryLiveData.value ?: "") ?: false
            }?.toMutableList() ?: mutableListOf())
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}