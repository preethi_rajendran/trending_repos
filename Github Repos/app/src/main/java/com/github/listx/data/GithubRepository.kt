package com.github.listx.data

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.github.listx.database.AppDatabase
import com.github.listx.database.GithubDao
import com.github.listx.model.database.GithubRepo
import com.github.listx.network.GithubRepoResponse
import com.github.listx.network.RetrofitHub
import com.github.listx.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class GithubRepository constructor(application: Application) {

    private var database: AppDatabase? = null
    private var dao: GithubDao? = null
    private val GITHUB_REPO_SINCE_INDEX = 0

    init {
        database = AppDatabase.getInstance(application)
        dao = database?.githubDao()
    }

    /**
     * Livedata of loading status
     */
    private val loadingState =
        MutableLiveData<Constants.State>().apply { value = Constants.State.NONE }

    /**
     * Return the loading status livedata
     */
    fun getLoading() = loadingState

    /**
     * Live data of error message
     */
    private val errorMessage = MutableLiveData<String>()

    /**
     * Returns the error live data
     */
    fun getErrorMessage() = errorMessage

    /**
     * Fetch the data from remote and stores it in room database
     */
    suspend fun getTrendingRepositories(): List<GithubRepo>? {
        updateLoadState(Constants.State.LOADING)
        return try {
            val trendingReposList =
                RetrofitHub.githubAPI.getTrendingRepositories(GITHUB_REPO_SINCE_INDEX)

            val offlineReposList = mutableListOf<GithubRepo>()

            trendingReposList.forEach {
                val repo = createRepoInstance(it)
                try {
                    val searchResponse =
                        RetrofitHub.githubAPI.searchRepository("repo:${it.fullName}")
                    val starCount = searchResponse.items.first().starCount
                    val language = searchResponse.items.first().language
                    repo.starCount = starCount
                    repo.language = language
                    offlineReposList.add(repo)
                } catch (exception: Exception) {
                    offlineReposList.add(repo)
                }
            }
            dao?.insertAllRepos(offlineReposList)
            updateLoadState(Constants.State.LOADED)
            offlineReposList
        } catch (exception: IOException) {
            updateLoadState(Constants.State.LOADED)
            updateErrorState(exception.localizedMessage)
            getRepositoriesList() ?: mutableListOf()
        } catch (exception: HttpException) {
            updateLoadState(Constants.State.LOADED)
            updateErrorState(exception.localizedMessage)
            getRepositoriesList() ?: mutableListOf()
        }
    }

    /**
     * Fetch all stored repostories from room database
     */
    suspend fun getRepositoriesList() = dao?.getAllRepos()

    /**
     * Create repository item and map it to api response, to store in database
     */
    private fun createRepoInstance(response: GithubRepoResponse): GithubRepo {
        return GithubRepo(
            response.id,
            response.name,
            response.fullName,
            response.description,
            response.owner.avatarUrl,
            0,
            "-",
        )
    }

    /**
     * Update loading status to show/hide progress bar
     */
    private suspend fun updateLoadState(state: Constants.State) {
        withContext(Dispatchers.Main) {
            loadingState.value = state
        }

    }

    /**
     * Update error status to display error message
     */
    private suspend fun updateErrorState(message: String?) {
        withContext(Dispatchers.Main) {
            message?.let {
                errorMessage.value = it
            }
        }

    }
}