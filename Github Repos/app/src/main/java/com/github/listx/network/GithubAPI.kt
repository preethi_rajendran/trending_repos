package com.github.listx.network

import retrofit2.http.GET
import retrofit2.http.Query

interface GithubAPI {
    /**
     * Fetch trending repostories from remote
     */
    @GET("/repositories")
    suspend fun getTrendingRepositories(@Query("since") since: Int): List<GithubRepoResponse>

    /**
     * Search the repository to fetch language and star count associated to it
     */
    @GET("/search/repositories")
    suspend fun searchRepository(@Query("q") query: String): SearchRepoResponse

}