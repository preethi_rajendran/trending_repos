package com.github.listx.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.github.listx.model.database.GithubRepo

@Dao
interface GithubDao {
    /**
     * Insert trending repositories list in table
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllRepos(list: List<GithubRepo>)

    /**
     * Fetch all stored repostories from table
     */
    @Query("Select * from github_repos")
    suspend fun getAllRepos(): List<GithubRepo>?

}