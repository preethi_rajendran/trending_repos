package com.github.listx.ui

import android.app.Activity
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.github.listx.R
import com.github.listx.data.GithubViewModel
import com.github.listx.databinding.ActivityTrendingReposListBinding
import com.github.listx.factory.ViewModelFactory
import com.github.listx.model.database.GithubRepo
import com.github.listx.ui.adapter.GithubReposAdapter
import com.github.listx.utils.Constants
import com.google.android.material.snackbar.Snackbar

class TrendingReposListActivity : AppCompatActivity() {

    /**
     * Github Repository list adapter object
     */
    private var adapter: GithubReposAdapter? = null

    /**
     * Github viewmodel object
     */
    private val viewModel: GithubViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelFactory.getInstance(application)
        ).get(GithubViewModel::class.java)
    }

    /**
     * Binding object of activity
     */
    private var _binding: ActivityTrendingReposListBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding =
            DataBindingUtil.setContentView(this, R.layout.activity_trending_repos_list)
        binding.apply {
            viewmodel = viewModel
            lifecycleOwner = this@TrendingReposListActivity
        }
        setContentView(binding.root)
        initViews()
        initDataObservers()
    }

    /**
     * Initialize views of the activity
     */
    private fun initViews() {
        setSupportActionBar(binding.toolbar)
        adapter = GithubReposAdapter()
        binding.reposList.adapter = adapter
        binding.reposList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                hideKeyboard(binding.root)
            }
        })
    }

    /**
     * initializes the required observers
     */
    private fun initDataObservers() {
        viewModel.repositoriesList.observe(this, Observer { reposList ->
            drawLayout(reposList)
            adapter?.submitList(reposList)
            binding.swipeRefreshLayout.isRefreshing = false
        })

        viewModel.filteredRepositoriesList().observe(this, Observer { reposList ->
            adapter?.submitList(reposList)
        })

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.getTrendingRepositories()
        }

        binding.tryAgain.setOnClickListener {
            binding.networkFailureGroup.visibility = View.GONE
            viewModel.getTrendingRepositories()
        }

        viewModel.loadingStatus.observe(this, Observer { state ->
            if (state == Constants.State.LOADING && !binding.swipeRefreshLayout.isRefreshing) binding.progressBar.visibility =
                View.VISIBLE
            else binding.progressBar.visibility = View.GONE
        })

        viewModel.errorMessage.observe(this, {
            showSnackBar(it, Color.RED)
        })

        viewModel.getTrendingRepositories()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        val item = menu?.findItem(R.id.search_bar);
        val searchView = item?.actionView as SearchView
        searchView.maxWidth = Int.MAX_VALUE
        searchView.queryHint = getString(R.string.search)
        searchView.setOnQueryTextFocusChangeListener { view, isFocused ->
            if (isFocused) showKeyboard(binding.root)
            else hideKeyboard(binding.root)

        }
        // search queryTextChange Listener
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus();
                viewModel.searchQueryLiveData.value = query
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                viewModel.searchQueryLiveData.value = query
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Toggle the layouts based on network availability and list
     */
    private fun drawLayout(reposList: List<GithubRepo>) {
        if (!isNetworkAvailable() && reposList.isEmpty()) {
            binding.networkFailureGroup.visibility = View.VISIBLE
            binding.swipeRefreshLayout.visibility = View.GONE
        } else {
            binding.networkFailureGroup.visibility = View.GONE
            binding.swipeRefreshLayout.visibility = View.VISIBLE
        }
    }

    /**
     * Check for network availability
     */
    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        return capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    }

    /**
     * Force hides the keyboard
     */
    fun hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /**
     * Shows the keyboard
     */
    fun showKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    /**
     * Displays error message in snackBar
     */
    private fun showSnackBar(string: String, color: Int = Color.RED) {
        val snackbar = Snackbar.make(binding.root, string, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(color)
        val textView =
            snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 16f
        snackbar.show()
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}