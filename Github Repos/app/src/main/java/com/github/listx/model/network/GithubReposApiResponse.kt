package com.github.listx.network

import com.google.gson.annotations.SerializedName

data class GithubRepoResponse(
    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,

    @SerializedName("full_name")
    var fullName: String,

    @SerializedName("description")
    var description: String,

    @SerializedName("stargazers_url")
    var stargazersUrl: String,

    @SerializedName("languages_url")
    var languagesUrl: String,

    @SerializedName("owner")
    var owner: Owner

)

data class Owner(
    @SerializedName("avatar_url")
    var avatarUrl: String
)

data class SearchRepoResponse(
    var items: List<ItemRepo>
)

data class ItemRepo(
    @SerializedName("stargazers_count")
    var starCount: Int,

    @SerializedName("language")
    var language: String,
)